# Load Monitor

## requirements

Create a simple web application that monitors load average on your machine:

* Collect the machine load (using “uptime” for example)
* Display in the application the key statistics as well as a history of load over the past 10 minutes in 10s intervals. We’d suggest a graphical representation using D3.js, but feel free to use another tool or representation if you prefer. Make it easy for the end-user to picture the situation!
* Make sure a user can keep the web page open and monitor the load on their machine.
* Whenever the load for the past 2 minutes exceeds 1 on average, add a message saying that “High load generated an alert - load = {value}, triggered at {time}”
* Whenever the load average drops again below 1 on average for the past 2 minutes, Add another message explaining when the alert recovered.
* Make sure all messages showing when alerting thresholds are crossed remain visible on the page for historical reasons.
* Write a test for the alerting logic
* Explain how you’d improve on this application design

## screenshot

![Imgur](http://i.imgur.com/zgg1Owt.png)

## implementation

I wasn't sure what was meant by "on your machine". I could have made a Node server with a websocket or REST endpoint for getting the *server*'s load avg (which would probably suffice for this demonstration since server and client would be run on the same machine). Instead, I used Electron with its built-in Node.js runtime as if it could be distributed and run on users's desktops. `os.loadavg()` should work on Linux and Mac (not Windows).

For some "key statistics", I chose uptime and used and unused memory.

I used React to render the views and Redux to manage state.

## how to run

`npm install`

`npm test`

`npm start`

## potential improvements

* requirements were specific about 1 being "high load" but that default could be changed to, say, the number of cores from `os.cpus().length` and be user configurable
* HH:MM on x-axis
* factor out constants into config settings (`ALERT_WINDOW_MS`, `MAX_HIST_LEN`, `MIN_ALERTABLE_LOAD`, `RECORDING_INTERVAL_MS`) or perhaps user configurable inputs
* logic in the `RECORD_MACHINE_LOAD` reducer is almost complex enough to warrant factoring out
* the domain of the y-axis is scaled to max out at the historical (since launch) max but maybe there's a better static choice
* use webpack (also for hot reloading in dev) and css-modules instead of the inline styles
* learn straight d3 instead of using react-d3 module
* test components with Enzyme
* make it prettier!
