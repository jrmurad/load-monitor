import os from 'os';

export function updateKeyStatistics() { // eslint-disable-line import/prefer-default-export
  return {
    type: 'UPDATE_KEY_STATISTICS',
    freemem: os.freemem(),
    totalmem: os.totalmem(),
    uptime: os.uptime(),
  };
}
