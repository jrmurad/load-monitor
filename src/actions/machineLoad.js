import os from 'os';

export function recordMachineLoad() { // eslint-disable-line import/prefer-default-export
  return {
    type: 'RECORD_MACHINE_LOAD',
    loadavg: os.loadavg()[0],
    timestamp: new Date(),
  };
}
