const ALERT_WINDOW_MS = 2 /* minutes */ * 60000 /* ms per min */;
const MAX_HIST_LEN = 10 /* minutes */ * 60000 /* ms per min */ / 10000 /* RECORDING_INTERVAL_MS */;
const MIN_ALERTABLE_LOAD = 1;

function isRecentlyHigh(history) {
  const latest = history[history.length - 1].timestamp;
  const inRange = history.filter(recording => latest - recording.timestamp <= ALERT_WINDOW_MS);
  const avg = inRange.reduce((sum, recording) => sum + recording.loadavg, 0) / inRange.length;
  return Boolean(avg > MIN_ALERTABLE_LOAD);
}

export default function machineLoad(state = {
  history: [],
  isHigh: false,
  messages: [],
  maxHistorical: 0,
}, action) {
  switch (action.type) {
    case 'RECORD_MACHINE_LOAD': {
      // append to history, shifting off least recent if # recordings exceeds MAX_HIST_LEN
      const recording = { loadavg: action.loadavg, timestamp: action.timestamp };
      const history = (state.history.length < MAX_HIST_LEN) ?
                      state.history.concat(recording) :
                      state.history.slice(1).concat(recording);

      // append a new message if the status has changed to/from high
      let message;
      const isHigh = isRecentlyHigh(history);

      if (isHigh !== state.isHigh) {
        const timeStr = action.timestamp.toLocaleTimeString();

        message = isHigh ?
                  `[${timeStr}] HIGH LOAD: ${action.loadavg}` :
                  `[${timeStr}] recovered from high load`;
      }

      return Object.assign({}, state, {
        messages: message ? state.messages.concat({
          id: state.messages.length + 1,
          text: message,
        }) : state.messages,
        history,
        isHigh,
        maxHistorical: Math.max(state.maxHistorical, action.loadavg),
      });
    }
    default:
      return state;
  }
}
