import reducer from './machineLoad';

const highLoadText = (ts, load) => `[${ts.toLocaleTimeString()}] HIGH LOAD: ${load}`;
const recoveryText = ts => `[${ts.toLocaleTimeString()}] recovered from high load`;

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual({
    history: [],
    isHigh: false,
    messages: [],
    maxHistorical: 0,
  });
});

it('should generate a "HIGH LOAD" message', () => {
  const timestamp = new Date();

  expect(
    reducer(undefined,
      { type: 'RECORD_MACHINE_LOAD', loadavg: 1.1, timestamp },
    ).messages
  ).toEqual([{
    id: 1,
    text: highLoadText(timestamp, 1.1),
  }]);
});

it('should generate a "recovered" message when avg is no longer above MIN_ALERTABLE_LOAD', () => {
  const highLoadTimestamp = new Date(new Date() - 10000);
  const recoveryTimestamp = new Date();

  expect(
    reducer({
      history: [{ loadavg: 1.1, timestamp: highLoadTimestamp }],
      isHigh: true,
      messages: [{ id: 1, text: '...' }],
    },
      { type: 'RECORD_MACHINE_LOAD', loadavg: 0.9, timestamp: recoveryTimestamp },
    ).messages
  ).toEqual([{
    id: 1,
    text: '...',
  }, {
    id: 2,
    text: recoveryText(recoveryTimestamp),
  }]);
});

it('should not generate any new message when avg remains above MIN_ALERTABLE_LOAD', () => {
  const highLoadTimestamp = new Date(new Date() - 10000);
  const recoveryTimestamp = new Date();

  expect(
    reducer({
      history: [{ loadavg: 1.1, timestamp: highLoadTimestamp }],
      isHigh: true,
      messages: [{ id: 1, text: '...' }],
    },
      { type: 'RECORD_MACHINE_LOAD', loadavg: 0.95, timestamp: recoveryTimestamp },
    ).messages
  ).toEqual([{
    id: 1,
    text: '...',
  }]);
});

it('should compute alerts based on ALERT_WINDOW_MS', () => {
  const highLoadTimestamp = new Date(new Date() - (120000 + 1000) /* 1s out of range */);
  const recoveryTimestamp = new Date();

  expect(
    reducer({
      history: [{ loadavg: 1.1, timestamp: highLoadTimestamp }],
      isHigh: true,
      messages: [{ id: 1, text: '...' }],
    },
      { type: 'RECORD_MACHINE_LOAD', loadavg: 0.95, timestamp: recoveryTimestamp },
    ).messages
  ).toEqual([{
    id: 1,
    text: '...',
  }, {
    id: 2,
    text: recoveryText(recoveryTimestamp),
  }]);
});
