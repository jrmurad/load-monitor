import { combineReducers } from 'redux';

import keyStatistics from './keyStatistics';
import machineLoad from './machineLoad';

const reducers = combineReducers({
  keyStatistics,
  machineLoad,
});

export default reducers;
