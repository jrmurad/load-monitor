export default function keyStatistics(state = {}, action) {
  switch (action.type) {
    case 'UPDATE_KEY_STATISTICS': {
      return Object.assign({}, state, {
        freemem: action.freemem,
        totalmem: action.totalmem,
        uptime: action.uptime,
      });
    }
    default:
      return state;
  }
}
