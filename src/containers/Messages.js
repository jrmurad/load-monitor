import { connect } from 'react-redux';

import Messages from '../components/Messages';

const mapStateToProps = ({ machineLoad }) => ({
  isActiveAlert: machineLoad.isHigh,
  messages: [...machineLoad.messages], // FIXME calling reverse on messages mutated store state???
});

export default connect(mapStateToProps)(Messages);
