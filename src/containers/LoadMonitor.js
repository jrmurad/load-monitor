import React from 'react';
import { connect } from 'react-redux';

import { recordMachineLoad } from '../actions/machineLoad';

import LoadMonitor from '../components/LoadMonitor';

const RECORDING_INTERVAL_MS = 10000;

const mapStateToProps = ({ machineLoad }) => ({
  data: machineLoad.history,
  yDomainMax: machineLoad.maxHistorical,
});

class LoadMonitorContainer extends React.Component {
  componentWillMount() {
    // initial recording
    this.props.dispatch(recordMachineLoad());

    // interval update
    this.interval = setInterval(() => {
      this.props.dispatch(recordMachineLoad());
    }, RECORDING_INTERVAL_MS);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <LoadMonitor {...this.props} />;
  }
}

LoadMonitorContainer.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(LoadMonitorContainer);
