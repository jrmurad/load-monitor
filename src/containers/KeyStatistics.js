import React from 'react';
import { connect } from 'react-redux';

import { updateKeyStatistics } from '../actions/keyStatistics';

import KeyStatistics from '../components/KeyStatistics';

const RECORDING_INTERVAL_MS = 1000;

const mapStateToProps = ({ keyStatistics }) => ({
  freemem: keyStatistics.freemem,
  totalmem: keyStatistics.totalmem,
  uptime: keyStatistics.uptime,
});

class KeyStatisticsContainer extends React.Component {
  componentWillMount() {
    // initialize
    this.props.dispatch(updateKeyStatistics());

    // interval update
    this.interval = setInterval(() => {
      this.props.dispatch(updateKeyStatistics());
    }, RECORDING_INTERVAL_MS);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <KeyStatistics {...this.props} />;
  }
}

KeyStatisticsContainer.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(KeyStatisticsContainer);
