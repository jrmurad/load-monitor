import React from 'react';

import filesize from 'filesize'; // for human readability
import humanizeDuration from 'humanize-duration';

const styles = {
  color: 'forestgreen',
};

const KeyStatistics = ({ freemem, totalmem, uptime }) =>
  <div style={styles}>
    up {humanizeDuration(1000 * uptime)}

    <br />

    PhysMem: {(totalmem - freemem) ? filesize(totalmem - freemem) : '--'} used,
    {' '}
    {freemem ? filesize(freemem) : '--'} unused
  </div>;

KeyStatistics.propTypes = {
  freemem: React.PropTypes.number,
  totalmem: React.PropTypes.number,
  uptime: React.PropTypes.number,
};

export default KeyStatistics;
