import React from 'react';

const styles = {
  maxHeight: 28,
  overflow: 'scroll',
};

const Messages = ({ isActiveAlert, messages }) =>
  <div style={styles}>
    {
      messages.reverse().map((message, i) =>
        <div
          key={message.id}
          style={(isActiveAlert && i === 0) ? { color: 'red' } : {}}
        >
          {message.text}
        </div>
      )
    }
  </div>;

Messages.propTypes = {
  isActiveAlert: React.PropTypes.bool,
  messages: React.PropTypes.arrayOf(React.PropTypes.shape({
    id: React.PropTypes.number.isRequired,
    text: React.PropTypes.string.isRequired,
  })).isRequired,
};

export default Messages;
