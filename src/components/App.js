import React from 'react';

import KeyStatistics from '../containers/KeyStatistics';
import LoadMonitor from '../containers/LoadMonitor';
import Messages from '../containers/Messages';

const App = () =>
  <div>
    <KeyStatistics />
    <LoadMonitor />
    <Messages />
  </div>;

export default App;
