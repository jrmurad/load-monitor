import React from 'react';

import { LineChart } from 'react-d3-basic';

const LoadMonitor = ({ data, yDomainMax }) =>
  <LineChart
    chartSeries={[{ field: 'loadavg' }]}
    data={data}
    height={200}
    margins={{ bottom: 5, top: 10, left: 25, right: 0 }}
    showLegend={false}
    width={375}
    x={recording => recording.timestamp}
    yDomain={[0, yDomainMax]}
  />;

LoadMonitor.propTypes = {
  data: React.PropTypes.arrayOf(React.PropTypes.shape({
    loadavg: React.PropTypes.number.isRequired,
    timestamp: React.PropTypes.instanceOf(Date).isRequired,
  })),
  yDomainMax: React.PropTypes.number,
};

export default LoadMonitor;
